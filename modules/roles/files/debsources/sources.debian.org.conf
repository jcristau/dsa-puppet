Use common-debian-service-https-redirect * sources.debian.org

WSGIDaemonProcess sources.debian.org user=nobody group=debsources home=/ processes=2 threads=5 maximum-requests=5000 inactivity-timeout=1800 umask=0077 display-name=wsgi-debsources.debian.org

<VirtualHost *:443>
    ServerName sources.debian.org
    ServerAdmin debian-admin@lists.debian.org

    Use common-debian-service-ssl sources.debian.org
    Use common-ssl-HSTS
    Use http-pkp-sources.debian.org

    <IfModule mod_userdir.c>
        UserDir disabled
    </IfModule>
    ErrorLog /var/log/apache2/sources.debian.org-error.log
    CustomLog /var/log/apache2/sources.debian.org-access.log privacy
    ServerSignature On

    <Directory /srv/sources.debian.org/sources>
        Options Indexes
        AllowOverride None
        DirectoryIndex disabled
        IndexOptions +ShowForbidden -FancyIndexing -HTMLTable +SuppressHTMLPreamble +XHTML
        # Note: mods-enabled/autoindex.conf has IndexIgnore set to
        # ignore all hidden files by default. For +ShowForbidden to be
        # practically useful, that entry should be commented out (or
        # reset'd with IndexOptionsReset, but only starting from Apache
        # 2.3).

        Require all granted
        <Files ~ "^\.ht">
            # override default (Debian) setting in /etc/apache2/apache2.conf
            # we want to list .htaccess and friends if they are part of packages.
            Require all granted
        </Files>

        SetHandler None
        # ForceType text/plain
    </Directory>

    <Directory /srv/sources.debian.org/static>
        Options FollowSymLinks
        Require all granted
    </Directory>

    Alias /data/ /srv/sources.debian.org/sources/
    Alias /static/ /srv/sources.debian.org/static/

    <Directory /srv/sources.debian.org/etc>
        <Files debsources.wsgi>
            Require all granted
        </Files>
    </Directory>

    WSGIScriptAlias / /srv/sources.debian.org/etc/debsources.wsgi
    WSGIProcessGroup sources.debian.org
</VirtualHost>

# vim: syntax=apache ts=4 sw=4 sts=4 sr et
